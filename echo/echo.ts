import { ConsoleReader } from './reader.ts';
import { ConsoleWriter } from './writer.ts'

export class Echo {
    static readonly DESCRIPTION: string = 'This is simple console echo. It responds you with your input.\n';
    private static readonly EXIT_TIP = 'Press "Ctrl+Z" or "Ctrl+C" to exit.\n';

    constructor(
        private readonly writer: ConsoleWriter,
        private readonly reader: ConsoleReader,
    ) {}

    async displayIntroduction(): Promise<void> {
        const constructor = this.constructor as typeof Echo;
        await this.writer.write(constructor.DESCRIPTION);
        await this.writer.write(constructor.EXIT_TIP);
    }

    protected async listen(): Promise<string> {
        await this.writer.write('Request: ');
        const request = await this.reader.read();
        return request;
    }

    protected async reply(response?: string): Promise<void> {
        await this.writer.write(`Response: ${response}`);
    }

    async echo(): Promise<void> {
        const request = await this.listen();
        await this.reply(request);
    }

    async startConversation(): Promise<void> {
        await this.echo();
        await this.startConversation();
    }
};

export class ReversedEcho extends Echo {
    static readonly DESCRIPTION: string = 'This is reversed console echo. It responds you with your input reversed.\n';
    static readonly END_OF_LINE = '\n';

    private static reverse = (string: string): string =>
        Array.from(string).reverse().join('');

    private static transform(request: string): string {
        const content = request.replace(this.END_OF_LINE, '');
        return this.reverse(content) + this.END_OF_LINE;
    }

    async echo(): Promise<void> {
        const request = await super.listen();
        await super.reply(ReversedEcho.transform(request));
    }
};
