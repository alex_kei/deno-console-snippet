export type DenoStdout =  Deno.Writer & Deno.WriterSync & Deno.Closer & { rid: number };

export class ConsoleWriter {
    private readonly textEncoder: TextEncoder;

    constructor(
        private readonly stdout: DenoStdout,
    ) {
        this.textEncoder = new TextEncoder();
    }

    async write(string?: string): Promise<void> {
        const buffer = this.textEncoder.encode(string);
        this.stdout.write(buffer);
    }
};
