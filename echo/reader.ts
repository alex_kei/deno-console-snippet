export type DenoStdin = Deno.Reader & Deno.ReaderSync & Deno.Closer & { rid: number };

export class ConsoleReader {
    private readonly textDecoder: TextDecoder;
    private buffer: Uint8Array;

    constructor(
        private readonly stdin: DenoStdin,
    ) {
        this.textDecoder = new TextDecoder();
        this.buffer = new Uint8Array(1024);
    }

    async read(): Promise<string> {
        const buffer = this.buffer;
        const bytes = Number(
            await this.stdin.read(buffer),
        );
        const text = this.textDecoder.decode(buffer.subarray(0, bytes));
        return text;
    }
};
