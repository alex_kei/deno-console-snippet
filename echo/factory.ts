import { ConsoleReader } from './reader.ts';
import { ConsoleWriter } from './writer.ts';
import { Echo, ReversedEcho } from './echo.ts';

const { stdin, stdout } = Deno;

export function createEcho(isReversed = false): Echo {
    const writer = new ConsoleWriter(stdout);
    const reader = new ConsoleReader(stdin);
    if(isReversed) {
        return new ReversedEcho(writer, reader);
    }
    return new Echo(writer, reader);
};
