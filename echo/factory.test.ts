import { assert } from "https://deno.land/std@0.103.0/testing/asserts.ts";
import { Echo, ReversedEcho } from './echo.ts';
import { createEcho } from './factory.ts';

Deno.test('Creates instance of Echo', () => {
    const echoer = createEcho();
    assert(echoer instanceof Echo);
});
Deno.test('Creates instance of ReversedEcho if param "isRevesed" set to true', () => {
    const echoer = createEcho(true);
    assert(echoer instanceof ReversedEcho);
});

Deno.test('Creates instance of Echo if param "isRevesed" set to false', () => {
    const echoer = createEcho(false);
    const instanceOfReversedEcho = echoer instanceof ReversedEcho;
    assert(echoer instanceof Echo);
    assert(!instanceOfReversedEcho);
});

Deno.test('Creates instance of Echo if param "isRevesed" is omitted', () => {
    const echoer = createEcho();
    const instanceOfReversedEcho = echoer instanceof ReversedEcho;
    assert(echoer instanceof Echo);
    assert(!instanceOfReversedEcho);
});