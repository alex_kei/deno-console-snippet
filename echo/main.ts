import { createEcho } from './factory.ts';

function parseCliArguments(): { isReversed: boolean} {
    const isReversed = Deno.args.includes('--reversed');
    return { isReversed };
}

(async function main() {
    const { isReversed } = parseCliArguments();
    const echoer = createEcho(isReversed);
    await echoer.displayIntroduction();
    await echoer.startConversation();
})();
