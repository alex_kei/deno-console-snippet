# Console scripts powered by Deno

## Prerequisites

* Install Deno:
```
curl -fsSL https://deno.land/x/install/install.sh | sh
```

## Run:

* Simple console echo
```
deno run simple-echo.ts
```

* Improved echo
```
deno run echo/main.ts
```

* Reversed echo
```
deno run echo/main.ts --reversed
```

## Create executables:

* Simple console echo
```
deno compile --output=simple-echo.bin simple-echo.ts
./simple-echo.bin
```

* Improved echo
```
deno compile --output=echo.bin echo/main.ts
./echo.bin
```

* Reversed echo
```
deno compile --output=reversed-echo.bin echo/main.ts --reversed
./reversed-echo.bin
```

## Testing:
```
deno test echo/
```