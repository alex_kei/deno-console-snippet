import { readLines } from 'https://deno.land/std@0.102.0/io/bufio.ts';

function displayIntroduction() {
  console.log('This is simple console echo. It responds you with your input.');
  console.log('Press "Ctrl+Z" or "Ctrl+C" to exit.');
}

(async function main() {
  displayIntroduction();
  for await (const line of readLines(Deno.stdin)) {
    console.log(line);
  }
})();